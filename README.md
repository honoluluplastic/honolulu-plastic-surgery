Dr. Wong's training was based on the reconstructive skills that were taught to him by some of the best plastic surgeons in the world.

Address: 1100 Ward Avenue, Suite 808, Honolulu, HI 96814, USA

Phone: 808-725-2114

Website: http://honoluluplasticsurgerycenter.com
